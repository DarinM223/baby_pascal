open Alcotest
open Baby_pascal.Code
open Baby_pascal.Cfg
open Baby_pascal.Intf

let code_of_array arr =
  arr |> Array.map (fun (op, a, b, r) -> { op; a; b; r }) |> CCVector.of_array

let test_example_1 () =
  let module Fresh = Fresh.Make () in
  let open Make (Fresh) (Sym.Make (Fresh)) in
  let expr =
    Baby_pascal.Ast.
      [
        Assign ("a", Bop (Add, Int 1, Bop (Mul, Int 2, Int 3)));
        If
          ( Bop (And, Bop (Eq, Var "a", Int 1), Bop (Lt, Var "a", Int 5)),
            [
              While
                ( Bop (And, Bop (Eq, Var "a", Int 1), Bop (Lt, Var "a", Int 5)),
                  [ Assign ("a", Bop (Add, Var "a", Int 1)) ] );
            ],
            [ Assign ("result", Int 60) ] );
      ]
  in
  let result = expr |> normalize |> CCVector.to_array in
  (check (array (testable pp_quad equal_quad)))
    "same array" result
    ([|
       (Mul, Const 2, Const 3, Temp 0);
       (Add, Const 1, Temp 0, Temp 1);
       (Assign, Temp 1, Empty, name 2);
       (Eq, name 2, Const 1, Const 5);
       (Goto, Const 15, Empty, Empty);
       (Lt, name 2, Const 5, Const 7);
       (Goto, Const 15, Empty, Empty);
       (Eq, name 2, Const 1, Const 9);
       (Goto, Const 16, Empty, Empty);
       (Lt, name 2, Const 5, Const 11);
       (Goto, Const 16, Empty, Empty);
       (Add, name 2, Const 1, Temp 3);
       (Assign, Temp 3, Empty, name 2);
       (Goto, Const 7, Empty, Empty);
       (Goto, Const 16, Empty, Empty);
       (Assign, Const 60, Empty, name 4);
       (Nop, Empty, Empty, Empty);
     |]
    |> Array.map (fun (op, a, b, r) -> { op; a; b; r }))

let test_figure_8_7 () =
  let example =
    [|
      (Assign, Const 1, Empty, name 1);
      (Assign, Const 1, Empty, name 2);
      (Mul, Const 10, name 1, Temp 1);
      (Add, Temp 1, name 2, Temp 2);
      (Mul, Const 8, Temp 2, Temp 3);
      (Sub, Temp 3, Const 88, Temp 4);
      (Assign, Const 0, Empty, name 3);
      (Add, name 2, Const 1, name 2);
      (Le, name 2, Const 10, Const 2);
      (Add, name 1, Const 1, name 1);
      (Le, name 1, Const 10, Const 1);
      (Assign, Const 1, Empty, name 1);
      (Sub, name 1, Const 1, Temp 5);
      (Mul, Const 88, Temp 5, Temp 6);
      (Assign, Const 1, Empty, name 3);
      (Add, name 1, Const 1, name 1);
      (Le, name 1, Const 10, Const 12);
    |]
    |> Array.map (fun (op, a, b, r) -> { op; a; b; r })
  in
  let module V = CCVector in
  let result =
    example |> V.of_array |> blocks_of_code |> M.bindings |> List.map snd
  in
  (check (list (module Block)))
    "same output" result
    [
      {
        phis = V.of_array [||];
        code = V.of_array [||];
        pcopies = V.of_array [||];
        pred = S.of_list [ 12 ];
        succ = S.of_list [];
      };
      {
        phis = V.of_array [||];
        code = V.of_array [||];
        pcopies = V.of_array [||];
        pred = S.of_list [];
        succ = S.of_list [ 0 ];
      };
      {
        phis = V.of_array [||];
        code = code_of_array [| (Assign, Const 1, Empty, name 1) |];
        pcopies = V.of_array [||];
        pred = S.of_list [ -1 ];
        succ = S.of_list [ 1 ];
      };
      {
        phis = V.of_array [||];
        code = code_of_array [| (Assign, Const 1, Empty, name 2) |];
        pcopies = V.of_array [||];
        pred = S.of_list [ 0; 9 ];
        succ = S.of_list [ 2 ];
      };
      {
        phis = V.of_array [||];
        code =
          code_of_array
            [|
              (Mul, Const 10, name 1, Temp 1);
              (Add, Temp 1, name 2, Temp 2);
              (Mul, Const 8, Temp 2, Temp 3);
              (Sub, Temp 3, Const 88, Temp 4);
              (Assign, Const 0, Empty, name 3);
              (Add, name 2, Const 1, name 2);
              (Le, name 2, Const 10, Const 2);
            |];
        pcopies = V.of_array [||];
        pred = S.of_list [ 1; 2 ];
        succ = S.of_list [ 2; 9 ];
      };
      {
        phis = V.of_array [||];
        code =
          code_of_array
            [|
              (Add, name 1, Const 1, name 1); (Le, name 1, Const 10, Const 1);
            |];
        pcopies = V.of_array [||];
        pred = S.of_list [ 2 ];
        succ = S.of_list [ 1; 11 ];
      };
      {
        phis = V.of_array [||];
        code = code_of_array [| (Assign, Const 1, Empty, name 1) |];
        pcopies = V.of_array [||];
        pred = S.of_list [ 9 ];
        succ = S.of_list [ 12 ];
      };
      {
        phis = V.of_array [||];
        code =
          code_of_array
            [|
              (Sub, name 1, Const 1, Temp 5);
              (Mul, Const 88, Temp 5, Temp 6);
              (Assign, Const 1, Empty, name 3);
              (Add, name 1, Const 1, name 1);
              (Le, name 1, Const 10, Const 12);
            |];
        pcopies = V.of_array [||];
        pred = S.of_list [ 11; 12 ];
        succ = S.of_list [ -2; 12 ];
      };
    ]

let _ =
  run "Test three address codegen"
    [
      ("Tests proper output", [ test_case "example 1" `Quick test_example_1 ]);
      ( "Tests flow graph creation",
        [
          test_case "modified figure 8.7 in dragon book" `Quick test_figure_8_7;
        ] );
    ]
